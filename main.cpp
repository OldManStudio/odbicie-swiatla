#include <windows.h>
#include<vector>

using namespace std;

vector <POINT> przeszkody;
HWND przycisk_dodaj, przycisk_wyczysc, pole_x1, pole_x2, pole_y1, pole_y2, info;

int GetIntFromEdit(HWND hEdit)
{
   // Funkcja GetWindowTextLength() pobiera z EDITa liczbe znak�w (plus jeden na znak zero)
   int textLength = GetWindowTextLength(hEdit) + 1;

   // Alokujemy pami�c potrzebn� nam do pobrania wartosci z EDITa
   char *textBuffer = new char[textLength];

   // Pobieramy wartosc z EDITa
   GetWindowText(hEdit, textBuffer, textLength );

   // Konwertujemy wartosc funkcj� atoi
   return atoi(textBuffer);
} 
/* This is where all the input to the window goes to */
LRESULT CALLBACK WndProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam) {
	switch(Message) {
		
		/* Upon destruction, tell the main thread to stop */
		case WM_DESTROY: {
			if((HWND)lParam == hwnd)
			{
				PostQuitMessage(0);
			}
			break;
		}
		
		case WM_COMMAND: {
			if(wParam == 1)
			{
				int x1 = GetIntFromEdit(pole_x1);
				int y1 = GetIntFromEdit(pole_y1);
				int x2 = GetIntFromEdit(pole_x2);
				int y2 = GetIntFromEdit(pole_y2);
				
				if(x1 == x2 || y1 == y2)
				{
					HDC hdc = GetDC(hwnd);
					HPEN czarny = CreatePen(PS_SOLID, 2, RGB(0, 0, 0));
					HPEN stary = (HPEN)SelectObject(hdc, czarny);
					
					MoveToEx(hdc, x1, y1, NULL);
					LineTo(hdc, x2, y2);
					
					SelectObject(hdc, stary);
					ReleaseDC(hwnd, hdc);
				}
			}
			break;
		}
		
		/* All other messages (a lot of them) are processed using default procedures */
		default:
			return DefWindowProc(hwnd, Message, wParam, lParam);
	}
	return 0;
}

/* The 'main' function of Win32 GUI programs: this is where execution starts */
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	WNDCLASSEX wc; /* A properties struct of our window */
	HWND hwnd; /* A 'HANDLE', hence the H, or a pointer to our window */
	MSG msg; /* A temporary location for all messages */

	/* zero out the struct and set the stuff we want to modify */
	memset(&wc,0,sizeof(wc));
	wc.cbSize		 = sizeof(WNDCLASSEX);
	wc.lpfnWndProc	 = WndProc; /* This is where we will send messages to */
	wc.hInstance	 = hInstance;
	wc.hCursor		 = LoadCursor(NULL, IDC_ARROW);
	
	/* White, COLOR_WINDOW is just a #define for a system color, try Ctrl+Clicking it */
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	wc.lpszClassName = "WindowClass";
	wc.hIcon		 = LoadIcon(NULL, IDI_APPLICATION); /* Load a standard icon */
	wc.hIconSm		 = LoadIcon(NULL, IDI_APPLICATION); /* use the name "A" to use the project icon */

	if(!RegisterClassEx(&wc)) {
		MessageBox(NULL, "Window Registration Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
		return 0;
	}

	hwnd = CreateWindowEx(WS_EX_CLIENTEDGE,"WindowClass","odbicie swiatla",WS_VISIBLE|WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, /* x */
		CW_USEDEFAULT, /* y */
		900, /* width */
		650, /* height */
		NULL,LoadMenu(hInstance, MAKEINTRESOURCE(200)),hInstance,NULL);

	pole_x1 = CreateWindowEx(WS_EX_CLIENTEDGE,"EDIT","x",WS_VISIBLE|WS_CHILD,
		10, /* x */
		10, /* y */
		80, /* width */
		20, /* height */
		hwnd,NULL,hInstance,NULL);
		
	pole_y1 = CreateWindowEx(WS_EX_CLIENTEDGE,"EDIT","y",WS_VISIBLE|WS_CHILD,
		100, /* x */
		10, /* y */
		80, /* width */
		20, /* height */
		hwnd,NULL,hInstance,NULL);

	pole_x2 = CreateWindowEx(WS_EX_CLIENTEDGE,"EDIT","x",WS_VISIBLE|WS_CHILD,
		10, /* x */
		50, /* y */
		80, /* width */
		20, /* height */
		hwnd,NULL,hInstance,NULL);

	pole_y2 = CreateWindowEx(WS_EX_CLIENTEDGE,"EDIT","y",WS_VISIBLE|WS_CHILD,
		100, /* x */
		50, /* y */
		80, /* width */
		20, /* height */
		hwnd,NULL,hInstance,NULL);

	przycisk_dodaj = CreateWindowEx(WS_EX_CLIENTEDGE,"BUTTON","Dodaj przeszkode",WS_VISIBLE|WS_CHILD,
		10, /* x */
		90, /* y */
		170, /* width */
		40, /* height */
		hwnd,(HMENU)1,hInstance,NULL);
		
	przycisk_wyczysc = CreateWindowEx(WS_EX_CLIENTEDGE,"BUTTON","Usun przeszkody",WS_VISIBLE|WS_CHILD,
		10, /* x */
		145, /* y */
		170, /* width */
		40, /* height */
		hwnd,(HMENU)2,hInstance,NULL);

	info = CreateWindowEx(WS_EX_CLIENTEDGE,"STATIC","Aby dodac przeszkode dla swiatla nalezy wpisac punk poczatkowy oraz punkt koncowy. Pomiedzy tymi punktami zostanie narysowana linia, ktora bedzie stanowic przeszkode dla swiatla.\nAby dodac punkt musi on byc rownolegly do jednej z krawedzi ekranu!",WS_VISIBLE|WS_CHILD,
		200, /* x */
		10, /* y */
		400, /* width */
		180, /* height */
		hwnd,NULL,hInstance,NULL);

	if(hwnd == NULL) {
		MessageBox(NULL, "Window Creation Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
		return 0;
	}

	/*
		This is the heart of our program where all input is processed and 
		sent to WndProc. Note that GetMessage blocks code flow until it receives something, so
		this loop will not produce unreasonably high CPU usage
	*/
	while(GetMessage(&msg, NULL, 0, 0) > 0) { /* If no error is received... */
		TranslateMessage(&msg); /* Translate key codes to chars if present */
		DispatchMessage(&msg); /* Send it to WndProc */
	}
	return msg.wParam;
}
